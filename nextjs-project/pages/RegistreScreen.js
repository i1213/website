import React, {useState, useEffect} from 'react'
import { FaUserPlus } from "react-icons/fa"
import { GoSignIn } from "react-icons/go"
import { Container, Form, Col, Row, Button} from 'react-bootstrap'
import { useRouter } from 'next/router'
import axios from 'axios'
import HomeScreen from './HomeScreen'
import {TiTickOutline } from 'react-icons/ti'
import Link from 'next/link'
import Message from './components/Message'

import styles from '../styles/styles.module.css'

const RegistreScreen = () => {

    const router = useRouter()


    const[FirstName, setFirstName] = useState('')
    const[LastName, setLastName] = useState('')    
    const[Email, setEmail] = useState('')
    const[Password, setPassword] = useState('')
    const[RepeatPass, setRepeatPass] = useState('')
    const [message, setMessage] = useState(null)
    const [error, setError] = useState(null)


    const [token, setToken] = useState('')
    async function register(FirstName, LastName, Email,Password) {

        axios({
            headers:   {
                'Content-Type': 'application/json'   ,
                'Accept' : 'application/json',
            },
            method: 'post',
            url: 'http://localhost:5000/api/users',
            data: {
                "FirstName": FirstName,
                "LastName": LastName,
                "Email": Email,
                "Password": Password
            }
        })
        .then(function (reponse) {
            setMessage('Register Success !')
            console.log(reponse)
            localStorage.setItem('userInfo', JSON.stringify(reponse.data.token))
            router.push('/HomeScreen')
        })
        .catch(function (erreur) {
            setError(erreur)
            console.log(erreur);
        });
}

const submitHandler = (e) => {
    e.preventDefault()
    if( RepeatPass !== Password ) {
        setError('Mot de passe Non Compatible')
    } else {
        register(FirstName,LastName, Email, Password)
    }
    

    }

    useEffect(() => {
        setToken(window.localStorage.getItem('userInfo'))
      }, [setToken])

    return (
        <main className="py-3">
            {message && <Message variant='success mt-2' > { message }</Message>}
            {error && <Message variant='danger mt-2'>{error}</Message>}
            {token ? <HomeScreen /> : (
            <Container >
                <Row >
                <Col></Col>
                <Col xs={6}>
                <div style={{display: 'flex',justifyContent:'center'}}>
                    <div className={styles.registerContainer}>
                        <FaUserPlus style={{fontSize: "35px"}}/>
                        <p><strong>Register</strong></p>
                        <p>Browse and find what you need.</p>
                    </div>
                    <Link href='/LoginScreen'>
                    <div className= {styles.signInContainer}>
                        <GoSignIn style={{fontSize: "35px"}} />
                        <div className={styles.iconsContainerRegister}>
                            <TiTickOutline style={{color: 'white'}}/>
                        </div>
                        <p><strong>Sign In</strong></p>
                        <p style= {{color: "#222222", opacity: "1"}}>Already have an account, the</p>
                    </div>   
                    </Link>

                </div>
                <Form onSubmit={submitHandler}>
                    <Row className="g-2">
                        <Col md>
                        <Form.Group controlId='FirstName' className='mt-4'>
                            <Form.Control
                                type='name'
                                placeholder='First Name*'
                                onChange={(e) => setFirstName(e.target.value)}
                                required
                            ></Form.Control>
                            </Form.Group>
                        </Col>
                        <Col md>  
                        <Form.Group controlId='LastName' className='mt-4'>
                    <Form.Control
                        type='name'
                        placeholder='Last Name*'
                        onChange={(e) => setLastName(e.target.value)}
                        required
                    ></Form.Control>
                    </Form.Group>
                        </Col>
                    </Row>
                    <Form.Group controlId='Email' className='mt-4'>
                    <Form.Control
                        type='email'
                        placeholder='Email*'
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    ></Form.Control>
                    </Form.Group>
                    <Form.Group controlId='Password' className='mt-4'>
                    <Form.Control
                        type='password'
                        placeholder='Password*'
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    ></Form.Control>
                    </Form.Group>
                    <Form.Group controlId='RepeatPassword' className='mt-4'>
                    <Form.Control
                        type='password'
                        placeholder='Repeat Password*'
                        onChange={(e) => setRepeatPass(e.target.value)}
                        required
                    ></Form.Control>
                    </Form.Group>
                    <div className={styles.submitContainer}>
                        <Button type='submit' variant='dark' className='mt-4'  style={{textAlign: 'cente'}}>
                            Submit
                        </Button>
                    </div>
                </Form>
                </Col>
                <Col>
                </Col>
                </Row>
            </Container>
             )}
        </main>
    )
}

export default RegistreScreen
