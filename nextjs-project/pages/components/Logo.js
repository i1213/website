import React from 'react'
import logo from '../../styles/logo.module.css'

const Logo = () => {
    return (
         <div className={logo.logoContainer}>
                        <div className={logo.logoParentRec}>
                            <div className={logo.topChildRec}></div>
                            <div className={logo.leftChildRec}></div>
                            <div className={logo.bottomChildRec}></div>
                            <div className={logo.rightChildRec}></div>
                        </div>
        </div>
    )
}

export default Logo
