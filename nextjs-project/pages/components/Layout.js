import { Container } from 'react-bootstrap';
import Footer from './Footer';
import Header from './Header';

const Layout = ({ children }) => (
    <>
        <Header />
        <div>
        {children}
        </div>
        <Footer />
    </>
)

export default Layout;