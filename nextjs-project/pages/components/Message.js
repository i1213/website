import React, { useState, useEffect } from 'react'

const Message = ({variant, children}) => {

            const [show, setShow] = useState(true);

            useEffect(() => {
                const timeId = setTimeout(() => {
                    setShow(false)
                }, 2000)
                return () => {
                    clearTimeout(timeId)
                }
                }, []);
                if(!show) {
                    return null;
                    }
            return (
                <>

                  <div className={`alert alert-${variant}`}>
                {children}
                </div>
        </>
    )
}

Message.defaultProps = {
    variant: 'info',
}

export default Message
