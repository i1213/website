import React from 'react'
import Link from 'next/link'
import { Navbar, Nav, Container, Image } from 'react-bootstrap'
import styles from '../../styles/styles.module.css'

const Header = ()  => {


    return (
        <>
        <div className='mt-1' style={{ display:'flex', justifyContent:'flex-end'}}>
            <Link href='/LoginScreen'>  
                <a style={{color: 'grey', marginRight:'1%', textDecoration: 'none'}} > Sign In </a>
            </Link>
            <Link href='/RegistreScreen'>
                
                <a style={{color: 'grey', textDecoration: 'none', marginRight: "1%"}}> Register </a> 
            </Link>
        </div>
        <header>
            <Navbar className='navbar  navbar-dark bg-dark mt-1' collapseOnSelect style={{fontFamily: 'Segoe UI', fontWeight:'bold', fontSize:'15px'}}>
                <Container>
                    <Link href='/HomeScreen'>
                    <Navbar.Brand > <Image src='logo.png' className= {styles.logoHeader}></Image> </Navbar.Brand>
                    </Link>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="me-auto">
                                <Link href='/HomeScreen'>
                                <Nav.Link href='/HomeScreen' >Home</Nav.Link></Link>   
                            <Link href='/'>
                                <Nav.Link > About Us  </Nav.Link>
                            </Link>
                            <Link href='/'>
                                <Nav.Link> Contact Us </Nav.Link>
                            </Link>
                            </Nav>
                    </Navbar.Collapse>        
                    <Nav className="mr-auto">
                            <Link href='/Profile'>
                                <Nav.Link> 
                                    <Image src='https://cdn-icons-png.flaticon.com/512/3135/3135715.png' className={styles.profileImg}></Image>
                                </Nav.Link>
                            </Link>
                            </Nav>                
                </Container>
            </Navbar>
        </header>
        </>
    )
}

export default Header