import React from 'react'
import { Container, Image } from 'react-bootstrap'
import styles from '../../styles/styles.module.css'

const Footer = () => {
    return (
        <footer className='bg-dark'   style={{height: "200px", marginTop: "20px", position: 'relative', bottom: '0', width:'100%'}}>
            <Container>
                <div className={styles.footerContainer}>
                    <Image src='logo.png' className={styles.logoFooter}/>
                </div>
                <div className={styles.copyright}>
                        &copy; All Rights Reserved.
                    </div>   
                
            </Container>
        </footer>
    )
}

export default Footer
