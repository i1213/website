import React, {useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import { FaUserPlus } from "react-icons/fa"
import { Form, Button, Row, Col } from 'react-bootstrap'
import { GoSignIn } from "react-icons/go"
import {TiTickOutline } from 'react-icons/ti'
import { useRouter } from 'next/router'
import axios from 'axios'
import HomeScreen from './HomeScreen'
import styles from '../styles/styles.module.css'
import Link from 'next/link'
import Message from './components/Message'

const LoginScreen = () => {

    const router = useRouter()

    const[Email,setEmail] = useState('')
    const[Password,setPassword] = useState('')
    const [message, setMessage] = useState(null)
    const [error, setError] = useState(null)


    const [token, setToken] = useState('')


    async function login(Email,Password) {

            axios({
                headers:   {
                    'Content-Type': 'application/json'   ,
                    'Accept' : 'application/json',
                },
                method: 'post',
                url: 'http://localhost:5000/api/users/login',
                data: {
                    "Email": Email,
                    "Password": Password
                }
            })
            .then(function (reponse) {
                console.log(reponse)
                setMessage('Login Success !')
                localStorage.setItem('userInfo', JSON.stringify(reponse.data.token))
                router.push('/HomeScreen')
            })
            .catch(function (erreur) {
                setError('Invalid Email or Password !')
                console.log(erreur);
            });
    }

    const submitHandler = (e) => {
        e.preventDefault()
        login(Email, Password)
        }

        useEffect(() => {
            setToken(window.localStorage.getItem('userInfo'))
          }, [setToken])
      
    return (
        <main className="mt-5">
            {message && <Message variant='success mt-2' > { message }</Message>}
            {error && <Message variant='danger mt-2'>{error}</Message>}
            {token ? <HomeScreen /> : (
            <Container>
            <Row >
                <Col></Col>
                <Col xs={6}>
                <div style={{display: 'flex',justifyContent:'center'}}>
                    <Link href='/RegistreScreen'>
                    <div className={styles.registerContainer}>
                        <FaUserPlus style={{fontSize: "35px"}}/>
                        <p><strong>Register</strong></p>
                        <p>Browse and find what you need.</p>
                    </div>
                    </Link>
                    <div className= {styles.signInContainer}>
                        <GoSignIn style={{fontSize: "35px"}} />
                        <div className={styles.iconsContainer}>
                            <TiTickOutline style={{color: 'white'}}/>
                        </div>
                        <p><strong>Sign In</strong></p>
                        <p style= {{color: "#222222", opacity: "1"}}>Already have an account, the</p>
                    </div>
                </div>
     <Form onSubmit={submitHandler}>
                    <Form.Group controlId='Email' className='mt-4'>
                        <Form.Control
                            type='email'
                            placeholder='Email*'
                            onChange={(e) => setEmail(e.target.value)}
                            required
                        ></Form.Control>
                    </Form.Group>

                    <Form.Group controlId='Password' className='mt-4'>
                        <Form.Control
                            type='password'
                            placeholder='Password*'
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        ></Form.Control>
                    </Form.Group>
                    <div className={styles.submitContainer}>
                        <Button type='submit' variant='dark' className='mt-4'  style={{textAlign: 'cente'}}>
                            Submit
                        </Button>
                    </div>

                </Form>
                </Col>
                <Col></Col>
            </Row>
                </Container> 
            ) 
             }


            
        </main>
    )
}

export default LoginScreen
