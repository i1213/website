import React, {useState, useEffect} from 'react'
import LoginScreen from './LoginScreen' 

import { Row, Col } from 'react-bootstrap'
import Logo from './components/Logo'

const HomeScreen = () => {


  
     const [token, setToken] = useState('')

    useEffect(() => {
        setToken(window.localStorage.getItem('userInfo'))
      }, [setToken])


    return (
        <>
        {token ?  
        <main>
            <div style={{position: 'relative'}}>
            <Row>
                <Col></Col>
                <Col className='mt-5' > 
                    <Logo />
                </Col>
                <Col></Col>
                </Row>
            </div>
        </main>

         : (
       <LoginScreen/>

        )}
 
        </>
    )
}

export default HomeScreen
